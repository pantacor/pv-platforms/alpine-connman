alpine-connman
==============

This is a platform container to manage the network layer of a pantavisor system.

It support kernel command line configuration.

## How to configurate via cmd

Inside the cmd we can add these configuration parameters

* pvnet_ssid: in here the name of the network ssid
* pvnet_pass: the network password
* pvnet_security: type of security (default: psk) 
* pvnet_type: type of network (ethernet, wifi, gadget, p2p)
* pvnet_ipv4: ip/network mask (default: dhcp)
* pvnet_ipv6: ip/network mask (default: dhcp)
