FROM --platform=$BUILDPLATFORM golang:1.22.6-alpine3.19 AS builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN apk update && \
	apk add make upx

WORKDIR /app
COPY pvwificonnect-dbus /app/

RUN /app/build.sh $TARGETPLATFORM

FROM alpine:3.17

COPY --from=builder /app/pvwificonnect-dbus-server /usr/bin/pvwificonnect-dbus-server

# INSTALL RUNTIME DEPENDENCIES
RUN apk add connman \
	openrc \
	dbus \
	wpa_supplicant \
	xdg-dbus-proxy \
	udev \
	iw \
	dnsmasq \
	sudo;

ADD files/ /

RUN rc-update add connman default; \
	rc-update add modules default; \
	rc-update add g_ether_setup default; \
	rc-update add g_ether_persist default; \
	rc-update add pvnet_load_configargs default; \
	rc-update add powersave_off default; \
	rc-update add pvdbus-proxy default; \
	rc-update add dnsmasq default; \
	rc-update add pvwificonnect-dbus default;

RUN sed -i 's/# NetworkInterfaceBlacklist = /NetworkInterfaceBlacklist = lxcbr,veth,/' /etc/connman/main.conf

VOLUME ["/var/pv"]
VOLUME ["/etc/modules-load.d"]

# CONFIGURATION

CMD [ "/sbin/init" ]
