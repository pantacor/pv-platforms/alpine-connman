#!/bin/sh
set -e

ci_to_docker() {
  case "$1" in
  "arm32v5"*)
    echo "linux/arm/v5"
    ;;
  "arm32v6"*)
    echo "linux/arm/v6"
    ;;
  "arm32v7"*)
    echo "linux/arm/v7"
    ;;
  "arm64v8"*)
    echo "linux/arm64"
    ;;
  "386"*)
    echo "linux/386"
    ;;
  "amd64"*)
    echo "linux/amd64"
    ;;
  "mips"*)
    echo "linux/mips"
    ;;
  "mipsle"*)
    echo linux/mipsle"
		;;
	"mips64"*)
		echo linux/mips64"
    ;;
  "mips64le"*)
    echo "linux/mips64le"
    ;;
  "riscv64"*)
    echo "linux/riscv64"
    ;;
  *)
	echo "$1"
	;;
  esac
}

platform=$(ci_to_docker $1)
target=""

echo "Building for $platform"
case "$platform" in
	"linux/arm/v6"*)
		target="linux-arm-6"
		;;
	"linux/arm/v7"*)
		target="linux-arm-7"
		;;
	"linux/arm")
		target="linux-arm"
		;;
	"linux/arm64"*)
		target="linux-arm64"
		;;
	"linux/386"*)
		target="linux-386"
		;;
	"linux/amd64"*)
		target="linux-amd64"
		;;
	"linux/mips"*)
		target="linux-mips"
		;;
	"linux/mipsle"*)
		target="linux-mipsle"
		;;
	"linux/mips64"*)
		target="linux-mips64"
		;;
	"linux/mips64le"*)
		target="linux-mips64le"
		;;
	"linux/riscv64"*)
		target="linux-riscv64"
		;;
	*)
		echo "Unknown machine type: $TARGETPLATFORM"
		exit 1
esac

go mod download
OUTPUT_DIR="./" OPTIMIZE=1 make build-$target
