package main

import (
	"fmt"
	"log"
	"os/exec"
	"strings"

	"github.com/godbus/dbus/v5"
)

const (
	dbusService = "org.pantacor.PvWificonnect"
	dbusPath    = "/org/pantacor/PvWificonnect"
	dbusIface   = "org.pantacor.PvWificonnect"
)

type PvWificonnect struct {
	iptablesPath string
	ipPath       string
}

const defaultAlpineIptablesPath = "/sbin/iptables"
const defaultAlpineIPPath = "/sbin/ip"

func PvWificonnectNew() (*PvWificonnect, error) {
	instance := &PvWificonnect{}
	iptablesPath, err := exec.LookPath("iptables")
	if err != nil {
		log.Printf("Failed search for iptables: %s", err)
		instance.iptablesPath = defaultAlpineIptablesPath
	}

	if iptablesPath == "" {
		log.Printf("iptablesPath not found")
		instance.iptablesPath = defaultAlpineIptablesPath
	} else {
		instance.iptablesPath = iptablesPath
	}

	ipPath, err := exec.LookPath("ip")
	if err != nil {
		log.Printf("Failed search for ip: %s", err)
		instance.ipPath = defaultAlpineIPPath
	}

	if ipPath == "" {
		log.Printf("Failed to add rule: ipPath not found")
		instance.ipPath = defaultAlpineIPPath
	} else {
		instance.ipPath = ipPath
	}

	return instance, nil
}

func (i *PvWificonnect) AddFwRule(rule string) (string, *dbus.Error) {
	args := strings.Split(rule, " ")
	cmd := exec.Command(i.iptablesPath, args...)
	if err := cmd.Run(); err != nil {
		log.Printf("Failed to add rule: %s -- %s", rule, err)
		return "", dbus.MakeFailedError(err)
	}

	log.Printf("Rule added successfully: %s", rule)
	return fmt.Sprintf("Rule added: %s", rule), nil
}

func (i *PvWificonnect) SetTetherIPv4(ip string) (string, *dbus.Error) {
	args := []string{
		"addr",
		"flush",
		"dev",
		"tether",
	}
	cmd := exec.Command(i.ipPath, args...)
	if err := cmd.Run(); err != nil {
		log.Printf("Failed to flush tether ip: %s -- %s", ip, err)
		return "", dbus.MakeFailedError(err)
	}

	args = []string{
		"addr",
		"add",
		ip,
		"dev",
		"tether",
	}
	cmd = exec.Command(i.ipPath, args...)
	if err := cmd.Run(); err != nil {
		log.Printf("Failed to set tether ip: %s -- %s", ip, err)
		return "", dbus.MakeFailedError(err)
	}

	log.Printf("Tether ip changed t: %s", ip)
	return fmt.Sprintf("Tether ip changed to: %s", ip), nil
}

func main() {
	// Connect to the system D-Bus
	conn, err := dbus.ConnectSystemBus()
	if err != nil {
		log.Fatalf("Failed to connect to system bus: %v", err)
	}
	defer conn.Close()

	// Request a name on the bus
	reply, err := conn.RequestName(dbusService, dbus.NameFlagDoNotQueue)
	if err != nil {
		log.Fatalf("Failed to request name: %v", err)
	}

	if reply != dbus.RequestNameReplyPrimaryOwner {
		log.Fatalf("Name already taken")
	}

	// Export the Iptables methods
	pvwcdbus, err := PvWificonnectNew()
	if err != nil {
		log.Fatalf("Failed create service: %v", err)
	}

	err = conn.Export(pvwcdbus, dbusPath, dbusIface)
	if err != nil {
		log.Fatalf("Failed to export methods: %v", err)
	}

	log.Printf("PvWificonnect D-Bus service running")

	// Wait forever, serving method calls
	select {}
}
