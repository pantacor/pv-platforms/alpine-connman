FROM --platform=$BUILDPLATFORM golang:1.22.6-alpine3.19 AS builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN apk update && \
	apk add make upx

WORKDIR /app
COPY pvwificonnect-dbus /app/

RUN /app/build.sh ${ARCH}

FROM alpine:3.17 AS qemu

RUN if [ -n "${QEMU_ARCH}" ]; then \
	wget -O /qemu-${QEMU_ARCH}-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-${QEMU_ARCH}-static; \
	else \
	echo '#!/bin/sh\n\ntrue' > /qemu-${QEMU_ARCH}-static; \
	fi; \
	chmod a+x /qemu-${QEMU_ARCH}-static

FROM ${ARCH}/alpine:3.17

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/
COPY --from=builder /app/pvwificonnect-dbus-server /usr/bin/pvwificonnect-dbus-server

# INSTALL RUNTIME DEPENDENCIES
RUN apk add
	connman \
	openrc \
	dbus \
	wpa_supplicant \
	xdg-dbus-proxy \
	udev \
	iw \
	dnsmasq \
	sudo;

ADD files/ /

RUN rc-update add connman default; \
	rc-update add modules default; \
	rc-update add g_ether_setup default; \
	rc-update add g_ether_persist default; \
	rc-update add pvnet_load_configargs default; \
	rc-update add powersave_off default; \
	rc-update add pvdbus-proxy default; \
	rc-update add dnsmasq default; \
	rc-update add pvwificonnect-dbus default;

RUN sed -i 's/# NetworkInterfaceBlacklist = /NetworkInterfaceBlacklist = lxcbr,veth,/' /etc/connman/main.conf

VOLUME ["/var/pv"]
VOLUME ["/etc/modules-load.d"]

# CONFIGURATION

CMD [ "/sbin/init" ]
